<?php
/*
Template Name: Template Sobre
*/


get_header(); ?>

<section class="back-sobre">
	<div class="container">
		<div class="col-md-12 col-lg-12">
			<?php the_content(); ?>
		</div>
	</div>
</section>
    
<section>
  	<div class="container">
    	<div class="row">
	    	<div class="col-md-12 col-lg-12">
	    		<h2 class="titulos-h2">EQUIPE</h2>    
	    	</div>
	    	<div class="lista-equipe">
	    		<?php
				if( have_rows('profissionais') ):
				    while ( have_rows('profissionais') ) : the_row();
				?>
				    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 equipe">

				    	<?php 
				    	$thumb = get_sub_field('foto_profissional');
						$size = 'thumb-profissional'; // (thumbnail, medium, large, full or custom size)
				    	echo wp_get_attachment_image( $thumb, $size ); ?>
					    <h3 ><?php the_sub_field('nome_profissional'); ?></h3>
					    <p><?php the_sub_field('resumo_profissional'); ?></p>
					</div>
				<?php
				    endwhile;
				else :
				endif;
				?>
	    	</div>
    	</div>
  	</div>
</section>
<section class="back-estrutura">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="titulos-h2">ESTRUTURA</h2>  
				<?php the_field('texto_estrutura'); ?>		
			</div>
			<div class="estrutura">				
				<?php 

				$images = get_field('galeria');

				if( $images ): ?>
				    <?php foreach( $images as $image ): ?>
				        <div class="col-xs-6 col-sm-4 item-galeria">
				            <a data-rel="lightbox-gallery-182" href="<?php echo $image['url']; ?>">
				                 <img class="img-responsive" src="<?php echo $image['sizes']['post-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				            </a>
				        </div>
				    <?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<section style="padding: 80px 0px 50px 0px;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="titulos-h2">DEPOIMENTOS</h2>
			</div>
			<div class="slider-depoimentos clear">
			<?php
			     global $post;
			     $args=array(
			       'posts_per_page'=>5,
			       'post_type' => 'depoimento',
			       'orderby'    => 'date',
			       'order'      => 'DESC'
			     );

			      
			     $my_query = new wp_query( $args );
			     
			     while( $my_query->have_posts() ) {
			     $my_query->the_post();
			     ?>
			    <div class="col-md-12 col-lg-12">
					<blockquote>
						"<?php the_content(); ?>"
					</blockquote>
				    <cite><?php the_title(); ?></cite>
				 </div>
				<?php }
				  wp_reset_query();
				?>  
			</div>
		</div>  
	</div>
</section>

<script>
	$('.slider-depoimentos').slick({
	dots: true,
	autoplay: true,
	infinite: true,
	speed: 1000,
	slidesToShow: 1,
	adaptiveHeight: false
	});
</script>
<?php
get_footer();
