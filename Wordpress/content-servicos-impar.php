<?php
/**
 * The template used for displaying page content.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
<section class="back-servicos row">
	<div class="container">
		<div class="row">
			<div class="hidden-xs hidden-sm  col-md-4 col-lg-4 servicos">
				<?php the_post_thumbnail('medium'); ?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 servicos">
				<h2 class="titulos-internos"><?php the_title(); ?></h2>
				<p><?php the_excerpt(); ?></p>
				<p><a href="<?php the_permalink(); ?>">Ver mais...</a></p>
			</div>
		</div>
	</div>
</section>
