<?php

require_once get_template_directory() . '/maps.php';

function register_my_menus() {
  register_nav_menus(
    array(

    	'menu-header' => __('Header'),
		'menu-footer' => __('Footer')
    )
  );
}
add_action( 'init', 'register_my_menus' );

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 380, 240, array( 'center', 'center')  ); // 50 pixels wide by 50 pixels tall, crop from the top left corner
add_image_size( 'thumb-quadrado', 250, 250, array( 'center', 'center' ) );
add_image_size( 'thumb-profissional', 360, 500, array( 'center', 'center' ) );


/**
Custom post Serviços
**/

function servicos_cpt() {
    $servico = new Odin_Post_Type(
	    'Serviços', // Nome (Singular) do Post Type.
	    'servicos' // Slug do Post Type.
	);
	$servico->set_labels(
	    array(
	        'name' => __( 'Serviços', 'odin' ),
	        'menu_name' => __( 'Serviços', 'odin' ),
	        'all_items' => __( 'Todos os Serviços', 'odin' ),
	        'add_new' => __( 'Adicionar Novo', 'odin' ),
	        'name_admin_bar' => __( 'Serviços', 'odin' ),
	        'add_new_item' => __( 'Adicionar Serviço', 'odin' ),
	        'edit_item' => __( 'Editar Serviço', 'odin' ),
	        'new_item' => __( 'Novo Serviço', 'odin' ),
	        'search_items' => __( 'Buscar Serviço	', 'odin' )
	    )
	);
	$servico->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail'),
	        'menu_position' => 5,
	        'menu_icon' => 'dashicons-clipboard',
	        'taxomy' => true
	    )
	);
}

add_action( 'init', 'servicos_cpt', 1 );


/**
Custom post Depoimentos
**/

function depoimento_cpt() {
    $depoimento = new Odin_Post_Type(
	    'Depoimentos', // Nome (Singular) do Post Type.
	    'depoimento' // Slug do Post Type.
	);
	$depoimento->set_labels(
	    array(
	        'name' => __( 'Depoimentos', 'odin' ),
	        'menu_name' => __( 'Depoimentos', 'odin' ),
	        'all_items' => __( 'Todos os Depoimentos', 'odin' ),
	        'add_new' => __( 'Adicionar Novo', 'odin' ),
	        'name_admin_bar' => __( 'Depoimentos', 'odin' ),
	        'add_new_item' => __( 'Adicionar Depoimento', 'odin' ),
	        'edit_item' => __( 'Editar Depoimento', 'odin' ),
	        'new_item' => __( 'Novo Depoimento', 'odin' ),
	        'search_items' => __( 'Buscar Depoimento	', 'odin' )
	    )
	);
	$depoimento->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail'),
	        'menu_position' => 5,
	        'menu_icon' => 'dashicons-admin-comments',
	        'taxomy' => true
	    )
	);
}
add_action( 'init', 'depoimento_cpt', 1 );
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

function carregarMapa($api, $endereco) {
	$gmaps = new gMaps($api);
	$dados = $gmaps->geolocal($endereco);
	return $dados;
}