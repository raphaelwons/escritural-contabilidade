<?php
// Converter endereço em latitude, longitude
class gMaps {
  // Host do GoogleMaps
  private $mapsHost = 'maps.google.com';
  // Sua Google Maps API Key
  public $mapsKey = '';

  function __construct($key = null) {
    if (!is_null($key)) {
      $this->mapsKey = $key;
    }
  }

  function carregaUrl($url) {
    if (function_exists('curl_init')) {
      $cURL = curl_init($url);
      curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, true);
      $resultado = curl_exec($cURL);
      curl_close($cURL);
    } else {
      $resultado = file_get_contents($url);
    }

    if (!$resultado) {
      return false;
      trigger_error('Não foi possível carregar o endereço: <strong>' . $url . '</strong>');
    } else {
      return $resultado;
    }
  }

  function geoLocal($endereco) {
    $endereco = urlencode($endereco);
    $url = $this->carregaUrl('http://maps.googleapis.com/maps/api/geocode/json?address='.$endereco);
    $dados_array = json_decode($url);

    if ($dados_array->status == 'OK') {
      $geodata = $dados_array->results[0]->geometry->location;
      $geodataquest = array('lat' => $geodata->lat, 'lon' => $geodata->lng);
      return $geodataquest;
    }else{
      return $geodataquest = array( 'error' =>'endereco não encontrado!');
    }
  }
}