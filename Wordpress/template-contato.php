<?php
/*
Template Name: Template Contato
*/


get_header(); ?>
<section class="">
	<div class="container-fluid">
			<?php if ( have_posts() ) : ?>
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
				  get_template_part( 'content-contato' );
				endwhile;

			endif;
		?>
	</div>
</section>
<?
get_footer();
