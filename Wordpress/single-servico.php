<?php
/*
Template Name: Template Serviço
*/


get_header(); ?>
<div class="container-full">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-12">
			<div class="banner-servicos-internos">
				<p class="texto-banner"><i>Serviços</i></p>
			</div>
		</div>
	</div>
</div>    

<div class="container-full">

	<section>
		<div class="container">
		  <div class="row-servicos">
		  	<div class="hidden-xs hidden-sm  col-md-4 col-lg-4 servicos">
		  		<?php the_post_thumbnail(); ?>
		  	</div>
		  	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 servicos">
		  		<h1 class="titulos-servicos"><?php the_title(); ?></h1>
		  		<p><?php the_excerpt(); ?></p>
		  	</div>
		  </div>
		</div>
	</section>

	<section style="padding: 20px 0px 20px 0px;">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-12 col-lg-12">
	        <h2 class="titulos-h2">QUAIS AS VANTAGENS?</h2>    
	      </div>
	      <div class="lista-vantagens">
	      	<?php if( have_rows('vantagens') ): ?>
			    <?php while( have_rows('vantagens') ): the_row(); ?>
			    <div class="col-sm-6">
			        <p class="check-size"><?php the_sub_field('vantagem'); ?></p>
			    </div>
			    <?php endwhile; ?>
			<?php endif; ?>
	      </div>
	    </div>
	  </div>
	</section>
</div>


<?php
get_footer();
