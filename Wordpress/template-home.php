<?php
/*
Template Name: Template Home
*/


get_header(); ?>
	
	<section>
		<div class="container-fluid whats">
		  <div class="row">
	        <div class="display-table">
			    <div class="display-cell">
			    	<img  src="<?php echo get_template_directory_uri(); ?>/assets/images/whatsapp.svg"  class="responsive-img whatsapp">
			    </div>
			    <div class="texto-whats display-cell">
			    	<span>Verifique a disponibilidade e entre em contato com facilidade através de nosso número WhatsApp, <?php the_field('whatsapp', 'option'); ?>.</span>
			    </div>
		  	</div>
		  </div>
		</div>
	</section>
	<div class="container blog">
	    <div class="row">
	    	<?php
	        global $post;
	        $args=array(
	          'posts_per_page'=>3,
	          'post_type' => 'post',
	          'orderby'    => 'date',
	          'order'      => 'DESC'
	        );

	         
	        $my_query = new wp_query( $args );
	        
	        while( $my_query->have_posts() ) {
	        $my_query->the_post();
	        ?>
	        <div class="col-md-4">
		        <h2><?php the_title(); ?></h2>
		        <p><?php the_excerpt(); ?></p>
		        <p><a href="<?php the_permalink(); ?>">Leia mais &raquo;</a></p>
		    </div>
			<?php }
			  wp_reset_query();
			?>
	    </div>
	</div>
	<div class="container-fluid">
	    <div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="call">
					  <p class="texto-banner">Estamos esperando sua visita!<br>qual sua necessidade?</p>
					<div class="wrapper">
					 	<div>
							<button class="botao"><a href="<?php  bloginfo('url'); ?>/contato" style="color:#fff;">Entre em contato</a></button>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
	<section class="back-mural">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<h2 class="titulos-h2">MURAL DE AVISOS</h2>
					<?php
					if( have_rows('avisos') ):
					    while ( have_rows('avisos') ) : the_row();
					?>
					    <div class="murais">
						    <h3 class="titulos";><?php the_sub_field('titulo_aviso'); ?></h3>
						    <p><?php the_sub_field('descricao_aviso'); ?></p>
						</div>
					<?php
					    endwhile;
					else :
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section style="margin-bottom: 100px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<h2 class="titulos-h2">LINKS ÚTEIS</h2>
					<?php
					if( have_rows('links') ):
					    while ( have_rows('links') ) : the_row();
					?>
					    <div class="murais">
						    <h3 class="titulos";><?php the_sub_field('titulo_link'); ?></h3>
						    <p><a href="<?php the_sub_field('url_link'); ?>"><?php the_sub_field('url_link'); ?></a></p>
						</div>
					<?php
					    endwhile;
					else :
					endif;
					?>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
