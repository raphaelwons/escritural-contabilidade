function initMap(lat, lon, endereco) {
  var latlng = new google.maps.LatLng(lat, lon);
  var map = new google.maps.Map(document.getElementById('map'), {
    scrollwheel: false,
    center: latlng,
    zoom: 18,
  });

  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h4 id="firstHeading" class="firstHeading">Escritural Contabilidade</h4>'+
      '<div id="bodyContent">'+ 
      '<p>' + endereco + '</p>' +
      '<p>contato@escrituralcontabilidade.com.br </p>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  var image = '../iconEndereco.svg';
  var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: image
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

}