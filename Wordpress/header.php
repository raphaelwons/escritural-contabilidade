<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Odin
 * @since 2.2.0
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<?php if ( ! get_option( 'site_icon' ) ) : ?>
		<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" rel="shortcut icon" />
	<?php endif; ?>
	<?php
		if ( is_page_template( 'template-sobre.php' ) ) {
	?>
		<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/slick/slick.min.js"></script>
	<?php
		}
	?>
	<?php
		if ( is_page_template( 'template-contato.php' ) ) {
	?>
		
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDvyN5EgI2YJzgfnCZgOUgWvuBwCJU6wE"></script>
  		<script src="<?php bloginfo('stylesheet_directory') ?>/assets/js/map.js"></script>
	<?php
		}
	?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
		    <div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		          <span class="sr-only">Toggle navigation</span>
		          <span class="icon-bar"></span>
		          <span class="icon-bar"></span>
		          <span class="icon-bar"></span>
		        </button>
				<a class="navbar-brand" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/LOGO-BRANCA.png"></a>
			</div>
		    <div id="navbar" class="navbar-collapse collapse">
		    	<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-header',
							'depth'          => 2,
							'container'      => false,
							'menu_class'     => 'nav navbar-nav',
							'fallback_cb'    => 'Odin_Bootstrap_Nav_Walker::fallback',
							'walker'         => new Odin_Bootstrap_Nav_Walker()
						)
					);
				?>
		    </div>
		</nav>
		<div class="container-fluid">
		  <div class="row">
		      <?php if(is_page()){ ?>
		      <div class="col-xs-12 col-sm-12 col-lg-12 banners" style="background-image: url('<?php echo the_field('fundo_header'); ?>')">
		        <div><h1 class="texto-banner"><i><?php the_title(); ?></i></h1></div>
		      </div>
		      <?php }
		       if ( is_post_type_archive('servicos')) { ?>
		       <div class="col-xs-12 col-sm-12 col-lg-12 banners banner-servicos">
			       <div><h1 class="texto-banner"><i>Serviços</i></h1></div>
			    </div>
		      <?php }
		       if ( is_singular('servicos') ) { ?>
		       <div class="col-xs-12 col-sm-12 col-lg-12 banners banner-servicos">
			       <div><h2 class="texto-banner"><i>Serviços</i></h2></div>
			    </div>
		      <?php }
		       if ( is_home() ) { ?>
		       <div class="col-xs-12 col-sm-12 col-lg-12 banners banner-blog">
			       <div><h1 class="texto-banner"><i>Blog</i></h1></div>
			    </div>
		       <?php }
		       if ( is_single() ) { ?>
		       <div class="col-xs-12 col-sm-12 col-lg-12 banners banner-blog">
			       <div><h2 class="texto-banner"><i>Blog</i></h2></div>
			    </div>
		       <?php } ?>


		  </div>
		</div>
	
