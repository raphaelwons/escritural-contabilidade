<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main div element.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
	<footer class="container-fluid rodape">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 infos">
				    <h4 class="titulos-rodape">MENU RÁPIDO</h4>
				   	<?php
						wp_nav_menu(
							array(
								'theme_location' => 'menu-footer',
								'depth'          => 2,
								'container'      => false,
								'menu_class'     => 'nav navbar-nav',
								'fallback_cb'    => 'Odin_Bootstrap_Nav_Walker::fallback',
								'walker'         => new Odin_Bootstrap_Nav_Walker()
							)
						);
					?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 infos">
					<h4 class="titulos-rodape">ONDE ESTAMOS</h4>
					<p><?php the_field('endereco', 'option'); ?></p>
					<p><?php the_field('e-mail', 'option'); ?></p>
					<?php
						if( have_rows('telefones', 'option') ):
						    while ( have_rows('telefones', 'option') ) : the_row();
						?>
						    <p><?php the_sub_field('telefone'); ?></p>
						<?php
						    endwhile;
						else :
						endif;
					?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 infos">
			    	<p><a href="<?php the_field('link_facebook', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.svg" class="icones">/EscrituralContabilidade</a></p>
			    	<p><a href="<?php the_field('link_instagram', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.svg" class="icones">@EscrituralContabilidade</a></p>
			    	<p><a href="<?php the_field('link_twitter', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.svg" class="icones">@EscrituralContabilidade</a></p>
			    	<p><a href="<?php the_field('link_youtube', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube.svg" class="icones">/EscrituralContabilidade</a></p>
				</div>
			</div>
		</div>
		<div class="creditos clear">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6">
					    <p>© Escritural Contabilidade 2016</p>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-6">
					    <p style="text-align: right;">Desenvolvido por <a target="_blank" href="http://digitalsquare.com.br/" style="color: #fff;">Digital Square</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	<?php wp_footer(); ?>
	
</body>
</html>
