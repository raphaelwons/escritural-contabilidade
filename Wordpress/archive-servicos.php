<?php



get_header(); ?>
<div class="container-fluid">
		<?php 
		global $count;
		$count = 1;
		if ( have_posts() ) : ?>
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
			if(($count % 2) != 0){
			  get_template_part( 'content-servicos-impar' );
			  $count++;
			}else{
			  get_template_part( 'content-servicos-par' );
			  $count++;
			}
			

			endwhile;

			// Page navigation.
			odin_paging_nav();

		endif;
	?>
</div>


<?php
get_footer();
