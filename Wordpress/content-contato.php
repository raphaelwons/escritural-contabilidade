<?php
/**
 * The template used for displaying page content.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
<section class="row section">
	<div class="container">
		<div class="row">
			<div class="">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<?php the_content(); ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 midias-sociais-contato">
					<h3>Telefones:</h3>
					<?php
						if( have_rows('telefones', 'option') ):
						    while ( have_rows('telefones', 'option') ) : the_row();
						?>
						    <p><?php the_sub_field('telefone'); ?></p>
						<?php
						    endwhile;
						else :
						endif;
					?>
					<hr>
				    <p><a href="<?php the_field('link_facebook', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.svg" class="icones">/EscrituralContabilidade</a></p>
					<p><a href="<?php the_field('link_instagram', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.svg" class="icones">@EscrituralContabilidade</a></p>
				    <p><a href="<?php the_field('link_twitter', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.svg" class="icones">@EscrituralContabilidade</a></p>
				   	<p><a href="<?php the_field('link_youtube', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube.svg" class="icones">/EscrituralContabilidade</a></p>
				</div>
			</div>
		</div>
	</div>
</section>
<hr>
<div class="row whats-contato">
  <div class="display-table">
	    <div class="display-cell">
	    	<img  src="<?php echo get_template_directory_uri(); ?>/assets/images/whatsapp.svg"  class="responsive-img whatsapp">
	    </div>
	    <div class="texto-whats display-cell">
	    	<span>Verifique a disponibilidade e entre em contato com facilidade através de nosso número WhatsApp, <?php the_field('whatsapp', 'option'); ?></span>
	    </div>
	</div>
</div>
<section id="localizacao" class="row">
	<div class="endereco-mapa"><h3><?php the_field('endereco', 'option'); ?></h3></div>
  <?php
  	$api = 'AIzaSyCKWk6aJttHc46UifyRW3gCTRp6vp8ZOH4';
  	$enderecoMatriz = get_field('endereco', 'option');
  	$coordenadasMatriz = carregarMapa($api, $enderecoMatriz);
	?>
	<script>
  		$(document).ready(function() {
	      	var latitude = <?php echo $coordenadasMatriz['lat']; ?>;
	      	var longitude = <?php echo $coordenadasMatriz['lon']; ?>;
	      	var logradouro = "<?php echo $enderecoMatriz; ?>";
	      	function show_map(){
	        	initMap(latitude, longitude, logradouro);
	      	};
	      	window.setTimeout( show_map, 2000 );
    	});
  	</script>

  <div id="map"></div>

</section>

